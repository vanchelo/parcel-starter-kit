console.log('Hello World');

const container = document.getElementById('container');

const render = (target, html) => target.innerHTML = html;

setTimeout(() => {
  render(container, 'Hello, World.');
}, 2000);
